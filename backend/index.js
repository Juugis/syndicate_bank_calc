const express = require('express')
var cors = require('cors')
const app = express()
const port = 8082
app.use(cors())
var clicks = {};

app.get('/save-clicks/', (req, res) => {
    let click = clicks[req.query.click]
    if(!click){
        clicks[req.query.click] = 1
    }else{
        clicks[req.query.click]++
    }
    res.send({ok:true})
})

app.get('/get-clicks/', (req, res) => {
    res.send(clicks)
})

app.listen(port, () => {
  console.log(`Listening http://localhost:${port}`)
})